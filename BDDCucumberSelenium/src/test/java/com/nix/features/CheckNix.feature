@nixCheck
Feature: Check Product, Blog and Contacts links

  Background:
    Given I am on 'NixSolutions' start page

    Scenario Outline: 001 Check Products link
      When I click on "<type>" button
      Then I see the correct "<name>" of the page
      Examples:
      | type      | name                                       |
      | Product   |   https://www.nixsolutions.com/products/   |
      | Blog      |   https://www.nixsolutions.com/blog/       |
      | Contacts  |   https://www.nixsolutions.com/contacts/   |


      Scenario: 001 Invalid message sending
        When I click on 'Send message' button without any text
        Then I get an error message