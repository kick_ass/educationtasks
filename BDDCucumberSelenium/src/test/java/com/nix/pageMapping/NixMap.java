package com.nix.pageMapping;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by mukhin on 9/30/2016.
 */
public class NixMap {
    public WebDriver driver;

    public NixMap(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = "/html/body/div/header/a/img")
    public WebElement Logo;

    @FindBy(xpath = ".//*[@id='menu-item-9373']/a")
    public WebElement Product;

    @FindBy(xpath = ".//*[@id='menu-item-6321']/a")
    public WebElement Blog;

    @FindBy(xpath = ".//*[@id='menu-item-1446']/a")
    public WebElement Contacts;

    @FindBy(xpath = ".//*[@id='gform_submit_button_1']")
    public WebElement SendMessageKey;

    @FindBy(xpath = ".//*[@id='gform_1']/div[1]")
    public WebElement ErrorMessage;

    public void clickOnButton(String buttonName) throws Exception {

        switch (buttonName) {
            case "Product": {
                Product.click();
                break;
            }
            case "Blog": {
                Blog.click();
                break;
            }
            case "Contacts": {
                Contacts.click();
                break;
            }
        }
    }

    public void clickOnSendKey () {
        SendMessageKey.click();
    }
}
