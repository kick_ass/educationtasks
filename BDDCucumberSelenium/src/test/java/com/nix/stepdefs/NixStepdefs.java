package com.nix.stepdefs;

import com.nix.Parser.Parse;
import com.nix.pageMapping.NixMap;
import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import com.nix.runner.Runner;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static junit.framework.Assert.*;

/**
 * Created by mukhin on 9/30/2016.
 */
public class NixStepdefs {
    private static NixMap nixMap;
    private static String URL;
    private static String PAGE;

    @Given("^I am on 'NixSolutions' start page$")
    public void iAmOnNixSolutionsStartPage() throws Throwable {
        if (!Runner.driver.getCurrentUrl().equals(Parse.URL))
            Runner.driver.get(Parse.URL);
        nixMap = new NixMap(Runner.driver);
    }


    @When("^I click on \"([^\"]*)\" button$")
    public void iClickOnButton(String buttonName) throws Throwable {
        nixMap.clickOnButton(buttonName);
    }


    @Then("^I see the correct \"([^\"]*)\" of the page$")
    public void iSeeTheCorrectOfThePage(String titleText) throws Throwable {
        assertEquals(titleText, Runner.driver.getCurrentUrl());
    }

    @When("^I click on 'Send message' button without any text$")
    public void iClickOnSendMessageButtonWithoutAnyText() throws Throwable {
        nixMap.clickOnSendKey();
    }

    @Then("^I get an error message$")
    public void iGetAnErrorMessage() throws Throwable {
        assertTrue("Something wrong!", nixMap.ErrorMessage.isDisplayed());
    }
}
