package com.nix.Parser;

import java.util.*;

import com.nix.runner.Runner;
import org.apache.commons.configuration.XMLConfiguration;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * Created by mukhin on 10/3/2016.
 */
public class Parse {
    public static final String BROWSER;
    public static final String URL;

    static {
        HashMap<String, String> config = read();
        URL = config.get("url");
        BROWSER = config.get("browser");

    }

    public static HashMap<String, String> read() {
        XMLConfiguration config = null;
        HashMap<String, String> map = new HashMap<>();
        try {
            config = new XMLConfiguration("config.xml");

            map.put("url", config.getString("url"));
            map.put("browser", config.getString("browser"));
            return map;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void chooseDriver() {
        switch (BROWSER) {
            case "Firefox": {
                Runner.driver = new FirefoxDriver();
                break;
            }
            case "Chrome": {
                System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
                Runner.driver = new ChromeDriver();
                break;
            }

            default: {
                Runner.driver = new FirefoxDriver();
            }
        }
    }
}
