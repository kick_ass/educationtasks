package domain;

/**
 * Created by mukhin on 10/17/2016.
 */
public class CheckData {
    private int valueId;
    private String inputValue;
    private String expectedValue;

    public void setValueId(int id) {
        this.valueId = id;
    }

    public int getValueId() {
        return valueId;
    }

    public void setInputValue(String inputValue) {
        this.inputValue = inputValue;
    }

    public String getInputValue() {
        return inputValue;
    }

    public void setExpectedValue (String expectedValue) {
        this.expectedValue = expectedValue;
    }

    public String getExpectedValue() {
        return expectedValue;
    }
}
