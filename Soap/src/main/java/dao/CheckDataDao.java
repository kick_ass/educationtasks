package dao;

import domain.CheckData;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by mukhin on 10/17/2016.
 */
public interface CheckDataDao {
    public List<CheckData> getAll() throws SQLException;
}
