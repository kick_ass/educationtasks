package dao;

import java.sql.Connection;

/**
 * Created by mukhin on 10/17/2016.
 */
public interface DaoFactory {
    public CheckDataDao getCheckDataDao(Connection connection);
}
