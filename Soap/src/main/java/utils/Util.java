package utils;

import domain.CheckData;
import h2.H2CheckDataDao;
import net.yandex.speller.services.spellservice.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mukhin on 10/17/2016.
 */

public class Util {
    private static Connection connection = null;
    private static H2CheckDataDao h2CheckDataDao = new H2CheckDataDao(connection);
    private static ObjectFactory ob = new ObjectFactory();
    private static SpellService spellService = new SpellService();
    private static SpellServiceSoap spellServiceSoap = spellService.getSpellServiceSoap();

    public static List<String> getAllResponces() throws SQLException {
        List<CheckData> list = h2CheckDataDao.getAll();
        List<String> responceList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            CheckTextRequest checkTextRequest = ob.createCheckTextRequest();
            checkTextRequest.setText(list.get(i).getInputValue());
            CheckTextResponse response = spellServiceSoap.checkText(checkTextRequest);
            responceList.add(response.getSpellResult().getError().get(0).getWord());
        }
        return responceList;
    }


}
