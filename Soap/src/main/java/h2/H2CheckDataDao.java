package h2;

import connection.ConnectionManager;
import dao.CheckDataDao;
import domain.CheckData;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mukhin on 10/17/2016.
 */
public class H2CheckDataDao implements CheckDataDao {
    private final Connection connection;

    public H2CheckDataDao(Connection connection) {
        this.connection = connection;
    }

   @Override
    public List<CheckData> getAll() throws SQLException {
                List<CheckData> list = new ArrayList<CheckData>();
                String h2 = "SELECT * FROM checkdata;";
                try {
                    PreparedStatement stmt = ConnectionManager.createConnection().prepareStatement(h2);
                    ResultSet rs = stmt.executeQuery();
                    while (rs.next()) {
                        CheckData c = new CheckData();
                        c.setValueId(rs.getInt("value_id"));
                        c.setInputValue(rs.getString("input_value"));
                        c.setExpectedValue(rs.getString("expected_value"));
                        list.add(c);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
        }
        return list;
    }
}
