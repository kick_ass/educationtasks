package h2;

import dao.CheckDataDao;
import dao.DaoFactory;
import java.sql.Connection;

/**
 * Created by mukhin on 10/17/2016.
 */
public class H2DaoFactory implements DaoFactory {
    public CheckDataDao getCheckDataDao(Connection connection) {
        return new H2CheckDataDao(connection);
    }
}
