package DBList;

import domain.CheckData;
import java.sql.SQLException;

/**
 * Created by mukhin on 10/18/2016.
 */
public class DBList {

    public static String getInputValue(CheckData obj) throws SQLException {
        return obj.getInputValue();
    }

    public static String getExpectedValue(CheckData obj) throws SQLException {
        return obj.getExpectedValue();
    }

//    public List<T> returnSet(String tableName) throws SQLException {
//        List<T> list = new ArrayList<>();
//        switch(tableName) {
//            case "checkdata": {
//                list = h2CheckDataDao.getAll();
//            }
//        }
//     }
}
