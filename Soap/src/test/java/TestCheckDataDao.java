import domain.CheckData;
import h2.H2CheckDataDao;
import org.junit.Assert;
import org.junit.Test;
import utils.Util;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by mukhin on 10/17/2016.
 */
public class TestCheckDataDao {
    Connection connection = null;
    H2CheckDataDao h2CheckDataDao = new H2CheckDataDao(connection);
    List<CheckData> list = h2CheckDataDao.getAll();

    public TestCheckDataDao() throws SQLException {
    }

    @Test
    public void testEquals() throws Exception {
        for (int i = 0; i < list.size(); i++) {
            Assert.assertEquals("!!!",list.get(i).getExpectedValue(), Util.getAllResponces().get(i));
        }
    }
}
