import DBList.DBList;
import GetRequests.GetRequests;
import connection.ConnectionManager;
import domain.CheckData;
import h2.H2CheckDataDao;
import org.junit.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by mukhin on 10/17/2016.
 */
public class TestCheckData {
    private static Connection connection = null;
    private static H2CheckDataDao h2CheckDataDao = new H2CheckDataDao(connection);
    private static List<CheckData> list;
    private GetRequests getRequests = new GetRequests();

    public TestCheckData() throws SQLException {
    }

    @BeforeClass
    public static void getListFronDB() throws SQLException {
        list = h2CheckDataDao.getAll();
    }

    @Test
    public void testEquals() throws Exception {
        for (int i = 0; i < list.size(); i++) {
            Assert.assertEquals("!!!", DBList.getExpectedValue(list.get(i)), getRequests.getRequestText(DBList.getInputValue(list.get(i))));
        }
    }

    @AfterClass
    public void kickConnection() throws SQLException {
        ConnectionManager.createConnection().close();
        System.out.println("Connection closed.");
    }
}
