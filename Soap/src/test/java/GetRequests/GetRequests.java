package GetRequests;

import net.yandex.speller.services.spellservice.*;
import org.junit.Before;

import java.sql.SQLException;

/**
 * Created by mukhin on 10/18/2016.
 */
public class GetRequests {
    private ObjectFactory ob = new ObjectFactory();
    private SpellService spellService = new SpellService();
    private SpellServiceSoap spellServiceSoap = spellService.getSpellServiceSoap();

    @Before
    public String getRequestText(String s) throws SQLException {
        CheckTextRequest checkTextRequest = ob.createCheckTextRequest();
        checkTextRequest.setText(s);
        CheckTextResponse response = spellServiceSoap.checkText(checkTextRequest);
        return response.getSpellResult().getError().get(0).getS().get(0);
    }
}
