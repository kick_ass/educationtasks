package com.company;

import java.util.Random;

/**
 * Created by mukhin on 9/28/2016.
 */
public class Calculator {
    private static int ARRAY_LENGTH = 5;
    private static int[] arr = new int[ARRAY_LENGTH];
    private static Random generator = new Random();
    public static int lastResult = 1;

    public static void initArray() {
        for (int i = 0; i < ARRAY_LENGTH; i++)
            arr[i] = generator.nextInt(10);
    }

    public static void printArray() {
        for (int i = 0; i < ARRAY_LENGTH; i++)
            System.out.print(arr[i] + " ");
        System.out.println();
    }

    public static int stMuktyArr() {
        int result = 1;
        for (int i = 0; i < ARRAY_LENGTH; i++) {
             result *= arr[i];
        }
        System.out.println("Static method result: ");
        return result;
    }

    public void multyArr() {
        for (int i = 0; i < ARRAY_LENGTH; i++) {
            lastResult *= arr[i];
        }
        System.out.println("Non static method result: " + lastResult);
    }
}
