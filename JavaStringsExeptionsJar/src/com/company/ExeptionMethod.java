package com.company;

import java.util.InputMismatchException;
import java.util.Scanner;
/**
 * Created by mukhin on 9/28/2016.
 */
public class ExeptionMethod {
    public static void exepMet () {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter a number: ");
        double a;
        try {
            a = in.nextDouble();
            int i = (int) a;
        } catch (InputMismatchException e) {
            System.out.println("Error! Must be a number!");
        }
    }
}
