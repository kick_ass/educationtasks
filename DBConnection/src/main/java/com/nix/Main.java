package com.nix;

import java.sql.*;

/**
 * Created by mukhin on 10/5/2016.
 */
public class Main {
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        Class.forName("org.h2.Driver");
        Connection connection = DriverManager.getConnection("jdbc:h2:~/test", "sa", "");
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT customer.first_name, customer.last_name, product.product_name, product.product_price\n" +
                "FROM customer\n" +
                "INNER JOIN product\n" +
                "ON customer.user_id = product.customer_id\n" +
                "WHERE product.product_name = 'Lion'\n" +
                "ORDER BY user_id;");

        while (rs.next()) {
            String firstName = rs.getString("first_name");
            String lastName = rs.getString("last_name");
            String productName = rs.getString("product_name");
            double productPrice = rs.getDouble("product_price");
            System.out.println(firstName + "  |  " + lastName + "  |  " + productName + "  |  " + productPrice);
        }
        connection.close();
	stmt.close();
    }
}
