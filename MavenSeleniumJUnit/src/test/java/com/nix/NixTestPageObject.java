package com.nix;

import com.thoughtworks.selenium.webdriven.commands.IsElementPresent;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import static junit.framework.Assert.*;
/**
 * Created by mukhin on 9/29/2016.
 */
public class NixTestPageObject {
    public static WebDriver driver;
    public static NixMap nixMap;
    public static String URL;

    @BeforeClass
    public static void setUp() throws Exception {
        driver = new FirefoxDriver();
        URL = "https://www.nixsolutions.com/";
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        nixMap = new NixMap(driver);
    }

    @Before
    public void browserBegin () throws Exception {
        if (driver == null) {
            driver = new FirefoxDriver();
        }
        driver.get(URL);
    }

    @AfterClass
    public static void theEnd () { driver.close(); }

    @Test
    public void checkLogo () { assertTrue("Logo is not displayed!", nixMap.Logo.isDisplayed()); }

    @Test
    public void checkProduct () {
        nixMap.clickOnProductButton();
        assertEquals("NIX Solutions Products for Mobile & Desktop | NIX Solutions", driver.getTitle());
    }

    @Test
    public void checkBlog () {
        nixMap.clickOnBlogButton();
        assertEquals("Web, Apps & Software Development Blog", driver.getTitle());
    }

    @Test
    public void checkContacts () {
        nixMap.clickOnContactsButton();
        assertEquals("NIX Solutions Contacts in Israel and Ukraine", driver.getTitle());
    }
}
