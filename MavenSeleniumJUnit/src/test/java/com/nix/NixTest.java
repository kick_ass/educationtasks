package com.nix;

import com.thoughtworks.selenium.webdriven.commands.IsElementPresent;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;

import java.util.concurrent.TimeUnit;

import static junit.framework.Assert.*;
/**
 * Created by mukhin on 9/29/2016.
 */
public class NixTest {
    static WebDriver driver;
    String title;

    @BeforeClass
    public static void browserBegin () {
        driver = new FirefoxDriver();
    }

    @Before
    public void goTo () {
        driver.navigate().to("https://www.nixsolutions.com/");
    }

    @Test
    public void goProductTest () {
        WebElement product = driver.findElement(By.xpath(".//*[@id='menu-item-9373']/a"));
        product.click();
        title = driver.getTitle();
        assertEquals("NIX Solutions Products for Mobile & Desktop | NIX Solutions", title);
    }

    @Test
    public void goBlogTest () {
        WebElement blog = driver.findElement(By.xpath(".//*[@id='menu-item-6321']/a"));
        blog.click();
        title = driver.getTitle();
        assertEquals("Web, Apps & Software Development Blog", title);
    }

    @Test
    public void goContactsTest () {
        WebElement contact = driver.findElement(By.xpath(".//*[@id='menu-item-1446']/a"));
        contact.click();
        title = driver.getTitle();
        assertEquals("NIX Solutions Contacts in Israel and Ukraine", title);
    }
}
