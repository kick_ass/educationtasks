package com.nix;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;

import java.util.concurrent.TimeUnit;

import static junit.framework.Assert.*;

/**
 * Created by mukhin on 9/29/2016.
 */
public class NixLogoTest {
    @Test
    public void testNixLogo() {
        WebDriver driver = new FirefoxDriver();
        driver.navigate().to("https://www.nixsolutions.com/");
        WebElement mainLogo = driver.findElement(By.xpath("/html/body/div/header/a/img"));
        assertTrue("Logo is not displayed!", mainLogo.isDisplayed());
        driver.close();
    }
}