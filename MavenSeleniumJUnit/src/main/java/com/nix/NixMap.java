package com.nix;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by mukhin on 9/29/2016.
 */
public class NixMap {
    public WebDriver driver;

    public NixMap(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy (xpath = "/html/body/div/header/a/img")
    public WebElement Logo;

    @FindBy (xpath = ".//*[@id='menu-item-9373']/a")
    public WebElement Product;

    @FindBy (xpath = ".//*[@id='menu-item-6321']/a")
    public WebElement Blog;

    @FindBy (xpath = ".//*[@id='menu-item-1446']/a")
    public WebElement Contacts;

    public void clickOnProductButton () {
        Product.click();
    }
    public void clickOnBlogButton () {
        Blog.click();
    }
    public void clickOnContactsButton () {
        Contacts.click();
    }
}
