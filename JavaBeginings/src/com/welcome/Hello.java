package com.welcome;

/**
 * Created by mukhin on 9/27/2016.
 */
public class Hello {
    private static String name;

    public void setupName(String name) {
        if (name != null)
            this.name = name;
        else
            System.out.println("Error! Please enter a name.");
    }

    public static void welcome () {
        System.out.println("Hello, " + name);
    }

    public static void byeBay () {
        System.out.println("Bye, " + name);
    }
}
