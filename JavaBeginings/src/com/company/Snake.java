package com.company;

import java.util.Scanner;

/**
 * Created by mukhin on 9/27/2016.
 */
public class Snake {
    private Scanner in = new Scanner(System.in);

    public void Snake () {
        System.out.print("Enter a number (from 1 to 5): ");
        int size = in.nextInt();
        if (size <= 5 && size >= 1) {
            int[][] arr = new int[size][size];
            int num = 1;

            for (int i = 0; i < size; i++) {
                for (int j = 0; j < size; j++) {
                    arr[i][j] = (num);
                    System.out.print(arr[i][j] + "\t");
                    if (num <= 8) {
                        num++;
                    }
                    else {
                        num = 1;
                    }
                }
                System.out.println();
            }
        }
        else {
            System.out.println("The number must be from 1 to 5!");
        }
            System.out.println("Done!");
    }
}
