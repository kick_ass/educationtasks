package com.company;

/**
 * Created by mukhin on 9/27/2016.
 */
public class Calculator {
    public static int Sum (int a, int b) {
        int result = 0;
        result = a + b;
        return result;
    }

    public static float Div (int a, int b) {
        float result = 0;
        result = a / b;
        return result;
    }

    public static int Mul (int a, int b) {
        int result = 0;
        result = a * b;
        return result;
    }
}
