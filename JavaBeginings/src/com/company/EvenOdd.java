package com.company;

/**
 * Created by mukhin on 9/27/2016.
 */
public class EvenOdd {
    public static void evenOdd (int a) {
        if (a <= 9 & a >= 0) {
            if (a % 2 == 0)
                System.out.println(a + " - It's even number");
            else
                System.out.println(a + " - It's odd number");
        }
        else {
            System.out.println("Error!");
        }
    }
}
