package com.nix;

import java.util.Collection;
import java.util.Iterator;

/**
 * Created by mukhin on 10/4/2016.
 */

public class ImplementClass implements Collection {
    public Object[] array = {1, 3, 2, null, 1};

    ImplementClass() {

    }

    public boolean add(Object o) {
        boolean add = false;
        if (!add) {
            for (int i = 0; i < array.length; i++) {
                if (array[i] == null) {
                    array[i] = o;
                    add = true;
                    break;
                }
            }
        }
        return add;
    }

    public int size() {
        int size = 0;
        for (int i = 0; i < array.length; i++) {
            size = i;
        }
        return size;
    }

    public boolean isEmpty() {
        boolean check = false;
        if (array == null) {
            check = true;
        }
        return check;
    }

    public void clear() {
        array = null;
    }

    public boolean remove(Object o) {
        boolean delete = false;
        if (!delete) {
            for (int i = 0; i < array.length; i++) {
                if (array[i] == o) {
                    array[i] = null;
                    delete = true;
                }
            }
        }
        return delete;
    }

    public boolean removeAll(Collection value) {
        boolean delete = false;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == value) {
                array[i] = null;
                delete = true;
            }
        }
        return delete;
    }

    public boolean retainAll(Collection value) {
        boolean retain = false;
        for (int i = 0; i < array.length; i++) {
            if (array[i] != value) {
                array[i] = null;
            }
        }
        return retain;
    }

    public boolean contains(Object value) {
        boolean contain = false;
        if (!contain) {
            for (int i = 0; i < array.length; i++) {
                if (array[i] == value)
                    contain = true;
            }
        }
        return contain;
    }

    public boolean containsAll(Collection value) {
        boolean contain = false;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == value)
                contain = true;
        }
        return contain;
    }


    public boolean addAll(Collection c) {
        return false;
    }

    public boolean equals(Object o) {
        boolean equal = false;
        for (int i = 0; i < array.length; i++) {
            if (o == array[i]) {
                equal = true;
            }
        }
        return equal;
    }

    public Object[] toArray() {
        return array;
    }

    public Object[] toArray(Object[] a) {
        return new Object[0];
    }

    public Iterator iterator() {
        return new IteratorImplement();
    }

    public int hashCode() {
        int result = 1;
        return result;
    }


}
