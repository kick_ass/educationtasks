package com.nix;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by mukhin on 9/29/2016.
 */

public class Snail {

    public static int[][] snailArray(int size) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите размерность матрицы: ");
        //int size = in.nextInt();
        int steps = size * size;
        int a[][] = new int[size][size];
        int maxValue = size * size, x = 0, y = size - 1;
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                a[i][j] = 0;
            }
        }
        boolean up = false, down = false, left = true, right = false;
        while (steps != 0) {
            while (left) {
                if (y > -1 && a[x][y] == 0) {
                    a[x][y--] = maxValue--;
                    steps--;
                } else {
                    left = false;
                    down = true;
                    y++;
                    x++;
                }

            }
            while (down) {
                if (x < a.length && a[x][y] == 0) {
                    a[x++][y] = maxValue--;
                    steps--;
                } else {
                    down = false;
                    right = true;
                    x--;
                    y++;
                }

            }
            while (right) {
                if (y < a[x].length && a[x][y] == 0) {
                    a[x][y++] = maxValue--;
                    steps--;
                } else {
                    right = false;
                    up = true;
                    y--;
                    x--;
                }
            }
            while (up) {
                if (x > -1 && a[x][y] == 0) {
                    a[x--][y] = maxValue--;
                    steps--;
                } else {
                    left = true;
                    up = false;
                    x++;
                    y--;
                }
            }
        }

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }

        return a;
    }

    public int searchMax(int[][] arr) {
        int max = 0;
        for (int i = 0; i < arr.length; i++)
            for (int j = 0; j < arr[i].length; j++) {
                if (max < arr[i][j]) {
                    max = arr[i][j];
                }
            }
            return max;
    }
}
