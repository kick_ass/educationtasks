package com.nix.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import java.util.concurrent.TimeUnit;

/**
 * Created by mukhin on 9/30/2016.
 */

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"html:target/cucumber-report/stackoverflow", "json:target/cucumber.json"},
        features = "src/test/java/com/nix/features",
        glue = "com/nix/stepdefs",
        tags = "@Snail" )

public class Runner {

}
