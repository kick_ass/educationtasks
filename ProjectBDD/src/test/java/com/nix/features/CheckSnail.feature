@Snail
Feature: Check Snail method

  Scenario: 001 Check that the max element is equal to size of matrix
    Given I run the Snail method
    When  I find the max element
    Then I see that the max element is equal to square of size that I entered