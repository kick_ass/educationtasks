package com.nix.stepdefs;

import com.nix.Snail;
import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import com.nix.runner.*;
import org.junit.Assert;

import static junit.framework.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by mukhin on 9/30/2016.
 */
public class MyStepdefs {
    private Snail snail = new Snail();
    private int[][] arr;
    private int max = 0;

    @Given("^I run the Snail method$")
    public void iRunTheSnailMethod() throws Throwable {
        arr = snail.snailArray(5);
    }

    @When("^I find the max element$")
    public void iFindTheMaxElement() throws Throwable {
        max = snail.searchMax(arr);
    }

    @Then("^I see that the max element is equal to square of size that I entered$")
    public void iSeeThatTheMaxElementIsEqualToSquareOfSizeThatIEntered() throws Throwable {
        assertEquals(25, max);
    }

}
