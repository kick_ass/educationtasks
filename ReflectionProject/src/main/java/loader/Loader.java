package loader;

import pathClassLoader.PathClassLoader;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mukhin on 10/19/2016.
 */
public class Loader extends ClassLoader implements PathClassLoader {
    private Map classesHash = new HashMap();
    private String[] classPath;

    public void setPath(String[] classPath) {
        this.classPath = classPath;
    }

    public String[] getPath() {
        return classPath;
    }

    protected synchronized Class loadClass(String name, boolean resolve) throws ClassNotFoundException {
        Class result = findClass(name);
        if (resolve) {
            resolveClass(result);
        }
        return result;
    }

    protected Class findClass(String name) throws ClassNotFoundException {
        Class result = (Class) classesHash.get(name);
        if (result != null) {
            return result;
        }

        File f = findFile(name.replace('.', '/'), ".class");

        if (f == null) {
            return findSystemClass(name);
        }

        try {
            byte[] classBytes = loadFileAsBytes(f);
            result = defineClass(name, classBytes, 0, classBytes.length);
        } catch (IOException e) {
            throw new ClassNotFoundException("Cannot load class" + name + ": " + e);
        } catch (ClassFormatError e) {
            throw new ClassNotFoundException("Format of class file incorrect for class " + name + ": " + e);
        }

        classesHash.put(name, result);
        return result;
    }

    protected URL findResource(String name) {
        File f = findFile(name, "");
        if (f == null) {
            return null;
        }
        try {
            return f.toURL();
        } catch (MalformedURLException e) {
            return null;
        }
    }

    private File findFile(String name, String extension) {
        for (int i = 0; i < classPath.length; i++) {
            File f = new File((new File(classPath[i])).getPath() + File.separatorChar + name.replace('/', File.separatorChar) + extension);
            if (f.exists())
                return f;
        }
        return null;
    }

    public static byte[] loadFileAsBytes(File file) throws IOException {
        byte[] result = new byte[(int) file.length()];
        FileInputStream f = new FileInputStream(file);
        try {
            f.read(result, 0, result.length);
        } finally {
            try {
                f.close();
            } catch (Exception e) {
                // Ignore exception
            }
            ;
        }
        return result;
    }
}


