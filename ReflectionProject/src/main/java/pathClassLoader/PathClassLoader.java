package pathClassLoader;

import java.security.PublicKey;

/**
 * Created by mukhin on 10/19/2016.
 */
public interface PathClassLoader {
    void setPath (String[] dir);
    String[] getPath();
}
