import loader.Loader;

import java.io.*;
import java.lang.reflect.Method;

/**
 * Created by mukhin on 10/20/2016.
 */
public class Test {
    public static void main(String[] args) throws Exception {
        Loader loader = new Loader();
        loader.setPath(new String[] {"E:\\New Folder\\"});
        Class clazz = Class.forName("TestModule", true, loader);
        Method[] method = clazz.getMethods();

        for (int i = 0; i < method.length; i++) {
            System.out.println(method[i].getName());
        }
    }
}
