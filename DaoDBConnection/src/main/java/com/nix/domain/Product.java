package com.nix.domain;

/**
 * Created by mukhin on 10/6/2016.
 */
public class Product {
    private int productId;
    private String productName;
    private double productPrice;

    public void setId(int id) {
        this.productId = id;
    }

    public void setProductName(String name) {
        this.productName = name;
    }

    public void setProductPrice(double price) {
        this.productPrice = price;
    }
}
