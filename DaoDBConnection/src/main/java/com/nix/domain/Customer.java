package com.nix.domain;

/**
 * Created by mukhin on 10/6/2016.
 */
public class Customer {
    private int userId;
    private String firstName;
    private String lastName;

    public void setUserId(int id) {
        this.userId = id;
    }

    public void setFirstName(String fName) {
        this.firstName = fName;
    }

    public void setLastName(String lName) {
        this.lastName = lName;
    }
}
