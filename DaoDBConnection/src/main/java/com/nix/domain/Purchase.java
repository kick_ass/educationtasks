package com.nix.domain;

/**
 * Created by mukhin on 10/6/2016.
 */
public class Purchase {
    private int purchaseId;
    private int customerId;
    private int productId;

    public void setPurchaseId(int id) {
        this.purchaseId = id;
    }

    public void setCustomerId(int id) {
        this.customerId = id;
    }

    public void setProductId(int id) {
        this.productId = id;
    }
}
