package com.nix.connection;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;
import org.h2.jdbcx.JdbcConnectionPool;


public class ConnectionManager {
    private static Properties properties = new Properties();
    private static JdbcConnectionPool connectionpool;
    private static Connection connection = null;

    public static Connection createConnection() {
        if (connectionpool == null) {
            properties = getCredentials();
            defineConnectionPool();
        }
        try{
            connection = connectionpool.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    private static void defineConnectionPool() {
        connectionpool = JdbcConnectionPool.create(properties.getProperty("Host"),
                properties.getProperty("UserName"),
                properties.getProperty("Password"));
    }

    public static Properties getCredentials(){
        try (FileInputStream fis = new FileInputStream(
                ConnectionManager.class.getClassLoader().getResource("jdbc.properties").getFile())) {
            properties.load(fis);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties;
    }
}