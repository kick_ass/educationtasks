package com.nix.h2;

import com.nix.connection.ConnectionManager;
import com.nix.dao.PurchaseDao;
import com.nix.domain.Purchase;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mukhin on 10/6/2016.
 */
public class H2PurchaseDao implements PurchaseDao {
    private final Connection connection;

    public H2PurchaseDao(Connection connection) {
        this.connection = connection;
    }

    public void delete(Purchase purchase) {

    }

    public void update(Purchase purchase) {

    }

    public Purchase create() {
        return null;
    }

    public List<Purchase> getAll() throws SQLException, IOException {
        String h2 = "SELECT * FROM purchase;";
        PreparedStatement stmt = ConnectionManager.createConnection().prepareStatement(h2);

        ResultSet rs = stmt.executeQuery();
        List<Purchase> list = new ArrayList<Purchase>();
        while(rs.next()) {
            Purchase p = new Purchase();
            p.setPurchaseId(rs.getInt("purchase_id"));
            p.setCustomerId(rs.getInt("customer_id"));
            p.setProductId(rs.getInt("product_id"));
            list.add(p);
        }
        return list;
    }
}
