package com.nix.h2;

import com.nix.connection.ConnectionManager;
import com.nix.dao.CustomerDao;
import com.nix.dao.DaoFactory;
import com.nix.dao.ProductDao;
import com.nix.dao.PurchaseDao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by mukhin on 10/6/2016.
 */
public class H2DaoFactory implements DaoFactory {

    public ProductDao getProductDao(Connection connection) {
        return new H2ProductDao(connection);
    }

    public CustomerDao getCustomerDao(Connection connection) {
        return new H2CustomerDao(connection);
    }

    public PurchaseDao getPurchaseDao(Connection connection) {
        return new H2PurchaseDao(connection);
    }
}
