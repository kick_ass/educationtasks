package com.nix.h2;

import com.nix.connection.ConnectionManager;
import com.nix.dao.CustomerDao;
import com.nix.domain.Customer;
import com.nix.domain.Product;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mukhin on 10/6/2016.
 */
public class H2CustomerDao implements CustomerDao {
    private final Connection connection;

    public Customer create() {
        return null;
    }

    public void update(Customer customer) {

    }

    public void delete(Customer customer) {

    }

    public List<Customer> getAll() throws SQLException {
        String h2 = "SELECT * FROM customer;";
        PreparedStatement stmt = ConnectionManager.createConnection().prepareStatement(h2);

        ResultSet rs = stmt.executeQuery();
        List<Customer> list = new ArrayList<Customer>();
        while (rs.next()) {
            Customer c = new Customer();
            c.setUserId(rs.getInt("user_id"));
            c.setFirstName(rs.getString("first_name"));
            c.setLastName(rs.getString("last_name"));
            list.add(c);
        }
        return list;
    }

    public H2CustomerDao (Connection connection) {
        this.connection = connection;
    }
}

