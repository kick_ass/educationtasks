package com.nix.h2;


import com.nix.connection.ConnectionManager;
import com.nix.dao.ProductDao;
import com.nix.domain.Product;
import com.nix.domain.Purchase;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mukhin on 10/6/2016.
 */
public class H2ProductDao implements ProductDao {
    private final Connection connection;

    public Product create() {
        return null;
    }

    public void update(Product product) {

    }

    public void delete(Product product) {

    }

    public List<Product> getAll() throws SQLException {
        String h2 = "SELECT * FROM product;";
        PreparedStatement stmt = ConnectionManager.createConnection().prepareStatement(h2);

        ResultSet rs = stmt.executeQuery();
        List<Product> list = new ArrayList<Product>();
        while(rs.next()) {
            Product p = new Product();
            p.setId(rs.getInt("product_id"));
            p.setProductName(rs.getString("product_name"));
            p.setProductPrice(rs.getDouble("product_price"));
            list.add(p);
        }
        return list;
    }

    public H2ProductDao (Connection connection) {
        this.connection = connection;
    }
}
