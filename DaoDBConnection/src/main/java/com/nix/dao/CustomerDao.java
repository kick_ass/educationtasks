package com.nix.dao;

import com.nix.domain.Customer;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by mukhin on 10/6/2016.
 */
public interface CustomerDao {
    /**
     * Создает новую запись и соответствующий ей объект
     */
    public Customer create();

    /**
     * Сохраняет состояние объекта group в базе данных
     */
    public void update(Customer customer);

    /**
     * Удаляет запись об объекте из базы данных
     */
    public void delete(Customer customer);

    /**
     * Возвращает список объектов соответствующих всем записям в базе данных
     */
    public List<Customer> getAll() throws SQLException;
}

