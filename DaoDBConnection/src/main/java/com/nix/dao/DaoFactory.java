package com.nix.dao;

import com.nix.connection.ConnectionManager;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by mukhin on 10/6/2016.
 */
public interface DaoFactory {
    public CustomerDao getCustomerDao(Connection connection);

    public ProductDao getProductDao(Connection connection);

    public PurchaseDao getPurchaseDao(Connection connection);
}

