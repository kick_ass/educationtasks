package com.nix.dao;

import com.nix.domain.Product;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by mukhin on 10/6/2016.
 */
public interface ProductDao {
    /**
     * Создает новую запись и соответствующий ей объект
     */
    public Product create();

    /**
     * Сохраняет состояние объекта group в базе данных
     */
    public void update(Product product);

    /**
     * Удаляет запись об объекте из базы данных
     */
    public void delete(Product product);

    /**
     * Возвращает список объектов соответствующих всем записям в базе данных
     */
    public List<Product> getAll() throws SQLException;
}

