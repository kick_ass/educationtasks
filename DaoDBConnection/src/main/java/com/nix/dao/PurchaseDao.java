package com.nix.dao;

import com.nix.domain.Customer;
import com.nix.domain.Purchase;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by mukhin on 10/6/2016.
 */
public interface PurchaseDao {

    public Purchase create();

    public void update(Purchase purchase);

    public void delete(Purchase purchase);

    public List<Purchase> getAll() throws SQLException, IOException;
}
