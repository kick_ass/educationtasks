package com.nix;

import com.nix.connection.ConnectionManager;
import com.nix.dao.CustomerDao;
import com.nix.dao.DaoFactory;
import com.nix.dao.ProductDao;
import com.nix.dao.PurchaseDao;
import com.nix.domain.Customer;
import com.nix.domain.Product;
import com.nix.domain.Purchase;
import com.nix.h2.H2DaoFactory;
import org.junit.*;
import com.nix.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by mukhin on 10/6/2016.
 */
public class DaoTest {
    @Test
    public void testGetAllPurchase() throws Exception {
        DaoFactory daoFactory = new H2DaoFactory();
        List<Purchase> list;
        Connection con = ConnectionManager.createConnection();
        PurchaseDao dao = daoFactory.getPurchaseDao(con);
        list = dao.getAll();
        Assert.assertNotNull(list);
        Assert.assertTrue(list.size() > 0);
    }

    @Test
    public void testGetAllCustomer() throws Exception {
        DaoFactory daoFactory = new H2DaoFactory();
        List<Customer> list;
        Connection con = ConnectionManager.createConnection();
        CustomerDao dao = daoFactory.getCustomerDao(con);
        list = dao.getAll();
        Assert.assertNotNull(list);
        Assert.assertTrue(list.size() > 0);
    }

    @Test
    public void testGetAllProduct() throws Exception {
        DaoFactory daoFactory = new H2DaoFactory();
        List<Product> list;
        Connection con = ConnectionManager.createConnection();
        ProductDao dao = daoFactory.getProductDao(con);
        list = dao.getAll();
        Assert.assertNotNull(list);
        Assert.assertTrue(list.size() > 0);
    }

}