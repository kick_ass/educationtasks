import com.company.EvenOdd;
import com.company.MultiplyArray;
import org.junit.Test;
import static junit.framework.Assert.*;

/**
 * Created by mukhin on 9/29/2016.
 */
public class MultyplyArrayTest {
    @Test
    public void testMultyply () {
        int[] arr = {2, 2, 2, 2, 2};
        int res = MultiplyArray.multiplyArray(arr);

        assertEquals(32, res);
    }
}
