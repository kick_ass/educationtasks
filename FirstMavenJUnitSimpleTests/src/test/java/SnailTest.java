import com.company.Snail;
import org.junit.Test;

import static junit.framework.Assert.*;

/**
 * Created by mukhin on 9/29/2016.
 */
public class SnailTest {
    @Test
    public void testSnail() throws Exception {
        int[][] arr = Snail.snailArray(5);
        int max = 0;
        for (int i = 0; i < arr.length; i++)
            for (int j = 0; j < arr[i].length; j++) {
                if (max < arr[i][j]) {
                    max = arr[i][j];
                }
            }
        assertEquals(25, max);
    }
}
