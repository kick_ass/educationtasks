import com.company.EvenOdd;
import org.junit.Test;
import static junit.framework.Assert.*;

/**
 * Created by mukhin on 9/29/2016.
 */

public class EvenOddTest {
    @Test
    public void testEven () throws Exception {
        String str = EvenOdd.evenOdd(4);

        assertEquals("- It's even number", str);

    }

    @Test
    public void testOdd () throws Exception {
       String str = EvenOdd.evenOdd(5);

        assertEquals("- It's odd number", str);
    }
}
