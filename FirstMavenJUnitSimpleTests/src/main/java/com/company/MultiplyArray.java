package com.company;

import java.util.Random;

/**
 * Created by mukhin on 9/27/2016.
 */
public class MultiplyArray {
    private static int ARRAY_LENGTH = 5;
    private static int[] arr = new int[ARRAY_LENGTH];
    private static Random generator = new Random();

    public static void initArray () {
        for (int i = 0; i < ARRAY_LENGTH; i++)
            arr[i] = generator.nextInt(10);
    }

    public static void printArray () {
        for (int i = 0; i < ARRAY_LENGTH; i++)
            System.out.print(arr[i] + " ");
    }

    public static int multiplyArray (int[] arr) {
        int result = 1;
        for (int i = 0; i < ARRAY_LENGTH; i++) {
            result *= arr[i];
        }
        System.out.println("Result: " + result);
        return result;
    }
}
